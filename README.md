# Test technique sf2 réalisé par Maxime PICARD

## Routes :
	- La route permettant de télécharger les commandes et afficher le résultat est /find-orders
	- La route permettant d'insérer une nouvelle commande est /new-order
	- La route d'API pour récupérer toute les commandes est /api/orders.json|yml
	- La route d'API pour récupérer une commande par son ID est /api/orders/{id_commande}.json|yml
	
## Fichiers :
	- Le paramètre url_orders se trouve dans app/config/parameters.yml et app/config/parameters.dist.yml
	- La classse récupérant le flux se trouve dans TestBundle/Classe
