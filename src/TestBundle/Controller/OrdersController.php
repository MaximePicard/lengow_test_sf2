<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use TestBundle\Entity\Commande;
use APY\DataGridBundle\Grid\Source\Entity;
use TestBundle\Form\Type\CommandeType;

/**
 * Controller gérant l'insertion des commandes
 *
 * @author Maxime PICARD <maxime@evolunium.fr>
 */
class OrdersController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @author Maxime PICARD <maxime@evolunium.fr>
     */
    public function findOrdersXmlAction()
    {
        $em = $this->getDoctrine()->getManager();

        // On boucle sur toute les commandes trouvées dans le fichier XML
        foreach ($this->get('test.lengow_test')->findOrdersXml()->orders->order as $order) {
            // Pour chaque commande, on crée une instance de l'entité Commande et on définit ses valeurs
            $commande = new Commande();
            $commande->setOrderAmount($order->order_amount);
            $commande->setOrderCommission($order->order_commission);
            $commande->setOrderCurrency($order->order_currency);
            $commande->setOrderId($order->order_id);
            $commande->setOrderPurchaseDateTime(new \DateTime($order->order_purchase_date.$order->order_purchase_heure));
            $commande->setOrderShipping($order->order_shipping);
            $commande->setOrderTax($order->order_tax);

            // On vérifie qu'aucune commande n'existe déjà avec l'ID de la commande qu'on va tenter d'insérer
            if (empty($em->getRepository('TestBundle:Commande')->findByOrderId($order->order_id))) {
                $em->persist($commande);
            } else {
                $this->get('logger')->warning("L'insertion d'une nouvelle commande a échouée car l'ID de cette commande existe déjà [" . $commande->getOrderId() . "]");
            }
        }

        // On enregistre toute les commandes en DB
        $em->flush();

        // On renvoi sur la route affichant toute les commandes
        return $this->redirectToRoute('show_orders');
    }

    /**
     * Affiche et gère la soumission du formulaire de création d'une commande
     * 
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @author Maxime PICARD <maxime@evolunium.fr>
     */
    public function newOrderAction(Request $request) {
        // On crée une nouvelle commande
        $commande = new Commande();
        $em = $this->getDoctrine()->getManager();

        // On hydrate le formulaire avec les données présentes dans la requête
        $form = $this->createForm(new CommandeType(), $commande);
        $form->handleRequest($request);

        // On vérifie qu'aucune commande n'existe déjà avec l'ID de la commande qu'on va tenter d'insérer
        // Si c'est le cas, on ajoute une erreur au formulaire pour refuser la validation
        if (!empty($em->getRepository('TestBundle:Commande')->findByOrderId($commande->getOrderId()))) {
            $this->get('logger')->warning("L'insertion d'une nouvelle commande a échouée car l'ID de cette commande existe déjà [" . $commande->getOrderId() . "]");
            $errorIdExistant = new FormError('Cet ID est déjà présent dans les commandes, choisissez en un autre');
            $form->get('orderId')->addError($errorIdExistant);
        }

        // On vérifie que le formulaire est envoyé et valide
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($commande);
            $em->flush();

            // On renvoi sur la route affichant toute les commandes
            return $this->redirectToRoute('show_orders');
        }

        return $this->render('TestBundle::newCommande.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    
    public function showOrdersAction() {
        // Création du tableau de résultat
        $commandes = new Entity('TestBundle:Commande');

        // Utilisation du service du bundle APYGrid pour insérer les commandes dans le tableau
        $grid = $this->get('grid');
        $grid->setSource($commandes);

        return $grid->getGridResponse('TestBundle::resultsCommandes.html.twig');
    }
}
