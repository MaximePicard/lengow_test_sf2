<?php

namespace TestBundle\Controller;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;

/**
 * @author Maxime PICARD <maxime@evolunium.fr>
 */
class ApiController extends FOSRestController
{
    /**
     * Retourne une commande
     *
     * @return  array
     *
     * @author Maxime PICARD <maxime@evolunium.fr>
     */
    public function getOrdersAction()
    {
        return $this->getDoctrine()->getManager()->getRepository('TestBundle:Commande')->findAll();
    }

    /**
     * Retourne une commande par son ID
     *
     * @param integer $id_commande
     *
     * @return  array
     *
     * @author Maxime PICARD <maxime@evolunium.fr>
     */
    public function getOrderAction($id_commande) {
        // Récupération de la commande par son ID
        $commande = $this->getDoctrine()->getManager()->getRepository('TestBundle:Commande')->findOneById($id_commande);

        // Vérification que la commande à bien été trouvée
        if (!is_object($commande)) {
            throw $this->createNotFoundException('Cette commande semble inexistante.');
        }

        return $commande;
    }
}
