<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use APY\DataGridBundle\Grid\Mapping as GRID;

/**
 * Commande
 *
 * @ORM\Table(uniqueConstraints={@UniqueConstraint(name="order_idx", columns={"order_id"})})
 * @ORM\Entity(repositoryClass="TestBundle\Entity\CommandeRepository")
 *
 * @GRID\Source(columns="id, marketplace, orderId, orderPurchaseDateTime, orderAmount, orderTax, orderShipping, orderCommission, orderCurrency")
 */
class Commande
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="marketplace", type="string", length=20, nullable=true)
     */
    private $marketplace;

    /**
     * @var string
     *
     * @ORM\Column(name="order_id", type="string", length=255, nullable=true)
     */
    private $orderId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="order_purchase_date_time", type="datetime", nullable=true)
     */
    private $orderPurchaseDateTime;

    /**
     * @var float
     *
     * @ORM\Column(name="order_amount", type="float", nullable=true)
     */
    private $orderAmount;

    /**
     * @var float
     *
     * @ORM\Column(name="order_tax", type="float", nullable=true)
     */
    private $orderTax;

    /**
     * @var float
     *
     * @ORM\Column(name="order_shipping", type="float", nullable=true)
     */
    private $orderShipping;

    /**
     * @var float
     *
     * @ORM\Column(name="order_commission", type="float", nullable=true)
     */
    private $orderCommission;

    /**
     * @var string
     *
     * @ORM\Column(name="order_currency", type="string", length=5, nullable=true)
     */
    private $orderCurrency;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marketplace
     *
     * @param string $marketplace
     * @return Commande
     */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return string 
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set orderId
     *
     * @param string $orderId
     * @return Commande
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return string 
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set orderPurchaseDate
     *
     * @param \DateTime $orderPurchaseDateTime
     * @return Commande
     */
    public function setOrderPurchaseDateTime($orderPurchaseDateTime)
    {
        $this->orderPurchaseDateTime = $orderPurchaseDateTime;

        return $this;
    }

    /**
     * Get orderPurchaseDateTime
     *
     * @return \DateTime
     */
    public function getOrderPurchaseDateTime()
    {
        return $this->orderPurchaseDateTime;
    }

    /**
     * Set orderAmount
     *
     * @param float $orderAmount
     * @return Commande
     */
    public function setOrderAmount($orderAmount)
    {
        $this->orderAmount = $orderAmount;

        return $this;
    }

    /**
     * Get orderAmount
     *
     * @return float 
     */
    public function getOrderAmount()
    {
        return $this->orderAmount;
    }

    /**
     * Set orderTax
     *
     * @param float $orderTax
     * @return Commande
     */
    public function setOrderTax($orderTax)
    {
        $this->orderTax = $orderTax;

        return $this;
    }

    /**
     * Get orderTax
     *
     * @return float 
     */
    public function getOrderTax()
    {
        return $this->orderTax;
    }

    /**
     * Set orderShipping
     *
     * @param float $orderShipping
     * @return Commande
     */
    public function setOrderShipping($orderShipping)
    {
        $this->orderShipping = $orderShipping;

        return $this;
    }

    /**
     * Get orderShipping
     *
     * @return float 
     */
    public function getOrderShipping()
    {
        return $this->orderShipping;
    }

    /**
     * Set orderCommission
     *
     * @param float $orderCommission
     * @return Commande
     */
    public function setOrderCommission($orderCommission)
    {
        $this->orderCommission = $orderCommission;

        return $this;
    }

    /**
     * Get orderCommission
     *
     * @return float 
     */
    public function getOrderCommission()
    {
        return $this->orderCommission;
    }

    /**
     * Set orderCurrency
     *
     * @param string $orderCurrency
     * @return Commande
     */
    public function setOrderCurrency($orderCurrency)
    {
        $this->orderCurrency = $orderCurrency;

        return $this;
    }

    /**
     * Get orderCurrency
     *
     * @return string 
     */
    public function getOrderCurrency()
    {
        return $this->orderCurrency;
    }
}
