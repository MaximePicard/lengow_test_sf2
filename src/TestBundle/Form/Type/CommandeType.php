<?php

namespace TestBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CommandeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('marketplace', 'text')
            ->add('orderId', 'text')
            ->add('orderPurchaseDateTime', 'datetime')
            ->add('orderAmount', 'money')
            ->add('orderTax', 'money')
            ->add('orderShipping', 'money')
            ->add('orderCommission', 'money')
            ->add('orderCurrency', 'currency')
            ->add('submit', 'submit')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'TestBundle\Entity\Commande'
        ]);
    }

    public function getBlockPrefix()
    {
        return 'test_bundle_commande_type';
    }

    public function getName() {
        return 'test_bundle_commande_type';
    }
}
