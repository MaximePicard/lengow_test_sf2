<?php

namespace TestBundle\Classe;

use Monolog\Logger;

/**
 * Classe permettant de récupérer les commandes au format XML depuis une URL
 *
 * @author Maxime PICARD <maxime@evolunium.fr>
 */
class FindOrders
{

    /**
     * @var string URL des commandes au format XML
     */
    private $urlOrders;

    /**
     * @var Logger Permet de tracer les appels et les retours des téléchargements du fichier xml
     */
    private $logger;

    /**
     * FindOrders constructor.
     *
     * @param string $urlOrders URL des commandes au format XML
     * @param Logger $logger    Permet de tracer les appels et les retours des téléchargements du fichier xml
     */
    public function __construct($urlOrders, Logger $logger)
    {
        $this->urlOrders = $urlOrders;
        $this->logger = $logger;
    }

    /**
     * Retourne les commandes présentes dans un fichier XML
     * 
     * @return \SimpleXMLElement
     *
     * @author Maxime PICARD <maxime@evolunium.fr>
     */
    public function findOrdersXml() {
        $this->logger->info("Les commandes viennent d'être téléchargées au format XML");
        return simplexml_load_file($this->urlOrders);
    }
}
